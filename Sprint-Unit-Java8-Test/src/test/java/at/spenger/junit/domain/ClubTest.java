package at.spenger.junit.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import ch.qos.logback.core.joran.action.Action;
import at.spenger.junit.domain.Person.Sex;

public class ClubTest {

	@Test
	@Ignore
	public void testEnter() {
		fail("Not yet implemented");
	}	

	@Test
	@Ignore
	public void testNumberOf() {
		Club c = new Club();
		
		int erg = c.numberOf();
		int erwartet = 0;
		assertEquals("Is the number of 0", erg, erwartet);
	}

	@Test
	@Ignore
	public void testGetAverage() {
		Club c = new Club();
		c = addPersons(c);
		
		
		float erg = c.getAverage();
		System.out.println("Average age: " + erg);
		boolean b = erg > 0;
		assertTrue(b);
	}
	
	@Test
	@Ignore
	public void testSortierenNachAlter() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		c.sortierenAlter();
		
		boolean b = c.getPersons().get(0).getBirthday().isBefore((c.getPersons().get(c.getPersons().size()-1).getBirthday()));
		System.out.println(c.print());
		
		assertEquals("The first Person should be older than the last one! ", true, b);
	}
	
	@Test
	@Ignore
	public void testSortierenNachName() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		c.sortierenName();
		
		int x = c.getPersons().get(0).getFullName().compareTo(c.getPersons().get(c.getPersons().size()-1).getFullName());
		System.out.println(c.print());
		boolean b = 0 > x;
		
		System.out.println(x);
		
		assertEquals("The first Person's name should be over the last ones! ", true, b);
	}
	@Test
	@Ignore
	public void testSortierenNachName2() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		c.sortName();
		
		int x = c.getPersons().get(0).getFullName().compareTo(c.getPersons().get(c.getPersons().size()-1).getFullName());
		System.out.println(c.print());
		boolean b = 0 > x;
		
		System.out.println("Nach name 2"
				+ "");
		
		assertEquals("The first Person's name should be over the last ones! ", true, b);
	}
	
	@Test
	@Ignore
	public void testPersonLeave() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		
		DateTimeFormatter fm = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate d = LocalDate.parse("1995-05-05", fm);
		Person p = new Person("Peter", "mom", d, Sex.MALE);
		c.enter(p);
		
		if(c.getPersons().size() == 0)
			c.enter(p);
		boolean b = c.leave(c.getPersons().get(0));
		boolean u = true;
		try{
			 u = c.leave(null);
		}
		catch(IllegalArgumentException e)
		{
			System.out.println(e.getMessage());
			u = false;
		}
		
		
		System.out.println(c.print());

		
		assertNotEquals("The booleans should not be the same! ", b, u);
	}
	
	
	@Test
	@Ignore
	public void testClearClub() {
		Club c = new Club();	
		c= addPersons(c);
		int i = c.getPersons().size();
		
		c.ClearClub();
		
		int f = c.getPersons().size();
		
		assertNotEquals("The amount should no be the same", i, f);
	}
	
	
	public Club addPersons(Club c)
	{
		
		DateTimeFormatter fm = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate d = LocalDate.parse("1995-05-05", fm);
		Person p = new Person("Peter", "mom", d, Sex.MALE);
		c.enter(p);
		
		d = LocalDate.parse("1996-05-02", fm);
		p = new Person("Horst", "Teaman", d, Sex.MALE);
		c.enter(p);
		
		d = LocalDate.parse("1999-08-12", fm);
		p = new Person("Kathi", "Burman", d, Sex.FEMALE);
		c.enter(p);
		
		d = LocalDate.parse("1991-01-02", fm);
		p = new Person("Lena", "Kamar", d, Sex.FEMALE);
		c.enter(p);
		
		return c;
	}
	
	@Test
	@Ignore
	public void testAlter()
	{
		System.out.println("TEST ALTER ");
		
		LocalDate d = LocalDate.parse("1950-09-27");
		Person p = new Person("Peter", "mom", d, Sex.MALE);
		
		Instant fixedInstant = Instant.parse("2010-01-02T11:00:00Z");
		Clock fixedClock = Clock.fixed(fixedInstant, ZoneId.of("Europe/Vienna"));
		
		ReflectionTestUtils.setField(p, "clock", fixedClock);
		
		int alter = p.getAge();
		System.out.println(alter);
		assertEquals(59, alter);
	}

	@Test
	public void Java8Durchschnittsalter()
	{
		Club c = new Club();
		c = addPersons(c);
		
		int count = (int) c.getPersons().stream().count();
		int ages = c.getPersons().stream().mapToInt(Person::getAge).sum();
		
		float f = ages / count;
		
		count = 0;
		ages = 0;
		
		for(Person p : c.getPersons())
		{
			count++;
			ages += p.getAge();
		}
		float ff = ages/count;
		
		assertEquals("The values should be equal", f, ff, 0.0001f);
	
	
	}
	
	@Test
	public void Java8SortierenName()
	{
		Club c = new Club();
		c = addPersons(c);
		
		List<Person> m8 = new ArrayList<Person>(c.getPersons()); //List was not editable. So I made myself one.
		
		m8.sort((p1,p2) -> p1.getLastName().compareTo(p2.getLastName()));
		
		int x = m8.get(0).getFullName().compareTo(m8.get(c.getPersons().size()-1).getFullName());
		
		assertTrue("The first Person's name should be over the last ones! ", (0 > x));
	}
	
	@Test
	public void Java8SortierenAlter()
	{
		Club c = new Club();
		c = addPersons(c);
		
		List<Person> m8 = new ArrayList<Person>(c.getPersons()); //Same thing as in Java8SortierenName
		
		m8.sort((Person p1, Person p2) -> p1.getBirthdayString().compareTo(p2.getBirthdayString()));
		
		boolean b = m8.get(0).getBirthday().isBefore((m8.get(c.getPersons().size()-1).getBirthday()));
		
		System.out.println(Arrays.toString(m8.toArray()));
		
		assertTrue("The first Person should be the oldest! ", b);
	}
	
	@Test
	public void Java8GruppierenGebJahr()
	{  
		Club c = new Club(); //
		c = addPersons(c);
		
		HashMap<Integer, List<Person>> map = new HashMap<Integer, List<Person>>();
		List<Person> m8 = new ArrayList<Person>(c.getPersons());
		
		m8.sort((Person p1, Person p2) -> p1.getJahr() - p2.getJahr());
		
		List<Person> m7 = null;
		
		for(Person p: m8)
		{
			 m7 = m8.stream().filter(p1 -> p1.getJahr() == p.getJahr()).collect(Collectors.toList());;
			for(Person pers: m7)
			{
				map.put(pers.getJahr(), m7);
			}
		}
		for(Integer i : map.keySet())
		{
			System.out.println("Jahr "+i+ " Persons: "+ Arrays.toString(map.get(i).toArray()) + System.lineSeparator());
		}
		
		
		assertEquals("There should be 2 people in 1999", 2, map.get(1999).size());
		
	}
	
	@Test
	public void Java8Austreten()
	{
		Club c = new Club();
		addPersons(c);
		
		long count = c.getPersons().stream().filter(p -> p.getLastName().equals("Kamar")).count();
		
		c.leave(c.getPersons().get(3));
		
		long countafter = c.getPersons().stream().filter(p -> p.getLastName().equals("Kamar")).count();
		
		assertNotEquals("They should not equal!", count, countafter);
		
	}
	
	@Test
	public void Java8Eintreten()
	{

		Club c = new Club();
		
		long count = c.getPersons().stream().filter(p -> p.getLastName().equals("Kamar")).count();
		
		addPersons(c);
		
		long countafter = c.getPersons().stream().filter(p -> p.getLastName().equals("Kamar")).count();
		
		assertNotEquals("They should not equal!", count, countafter);
	}
	
	@Test
	public void Java8AnzahlMaenner()
	{
		Club c = new Club();
		addPersons(c);
		
		long count = c.getPersons().stream().filter(p -> p.getSex().equals(Sex.MALE)).count();
		assertEquals("There should be to men in the club", count, 2);
	}
	
}
